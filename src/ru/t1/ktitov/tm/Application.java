package ru.t1.ktitov.tm;

import ru.t1.ktitov.tm.constant.TerminalConst;

import static ru.t1.ktitov.tm.constant.TerminalConst.*;

public class Application {

    // version, about, help
    public static void main(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void showErrorArgument() {
        System.err.println("Error! Not supported argument");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Titov Kirill");
        System.out.println("E-mail: kir.titoff@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application version \n", VERSION);
        System.out.printf("%s - Show developer info \n", ABOUT);
        System.out.printf("%s - Show application commands \n", HELP);
    }

}
